/* eslint-disable no-unused-vars */
import Vue from "vue";
import Vuex from "vuex";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import { auth } from "../main";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: null,
    status: null,
    error: null,
  },
  mutations: {
    SET_USER(state, payload) {
      state.user = payload;
    },

    REMOVE_USER(state) {
      state.user = null;
    },

    SET_STATUS(state, payload) {
      state.status = payload;
    },

    SET_ERROR(state, payload) {
      state.error = payload;
    },
  },
  getters: {
    status(state) {
      return state.status;
    },

    user(state) {
      return state.user;
    },

    error(state) {
      return state.error;
    },
  },
  actions: {
    async signUpAction({ commit }, payload) {
      commit("SET_STATUS", "loading");
      try {
        const response = await createUserWithEmailAndPassword(
          auth,
          payload.email,
          payload.password
        );
        alert("success");
        commit("SET_USER", response.user.uid);
        commit("SET_STATUS", "success");
        commit("SET_ERROR", null);
      } catch (error) {
        commit("SET_STATUS", "failure");
        commit("SET_ERROR", error.message);
      }
    },

    async signInAction({ commit }, payload) {
      try {
        const response = await signInWithEmailAndPassword(
          auth,
          payload.email,
          payload.password
        );
        commit("SET_USER", response.user.uid);
        commit("SET_STATUS", "success");
        commit("SET_ERROR", null);
      } catch (error) {
        commit("SET_STATUS", "failure");
        commit("SET_ERROR", error.message);
      }
    },

    async signOutAction({ commit }) {
      try {
        await signOut(auth);
        commit("SET_USER", null);
        commit("SET_STATUS", "success");
        commit("SET_ERROR", null);
      } catch (error) {
        commit("SET_STATUS", "failure");
        commit("SET_ERROR", error.message);
      }
    },
    async checkAuthState({ commit }) {
      try {
        const user = auth.currentUser;
        if (user) {
          commit("SET_USER", user.uid);
        }
      } catch (error) {
        console.log(error);
      }
    },

    // Listen for changes in the user's authentication status
    async subscribeToAuthChanges({ commit }) {
      auth.onAuthStateChanged((user) => {
        if (user) {
          commit("SET_USER", user.uid);
        } else {
          commit("SET_USER", null);
        }
      });
    },
  },
  // Call the checkAuthState and subscribeToAuthChanges actions on app load
  created() {
    this.dispatch("checkAuthState");
    this.dispatch("subscribeToAuthChanges");
  },
});
