import Vue from "vue";
import VueRouter from "vue-router";
import SettingPage from "@/views/SettingPage";
import SignIn from "@/views/SignIn";
import SignUp from "@/views/SignUp";
import HeightTracker from "@/views/HeightTracker";
import HeightDatabase from "@/views/HeightDatabase";

Vue.use(VueRouter);

const routes = [
  {
    path: "/signin",
    name: "SignIn",
    component: SignIn,
  },
  {
    path: "/signup",
    name: "SignUp",
    component: SignUp,
  },
  {
    path: "/",
    name: "SettingPage",
    component: SettingPage,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/heighttracker",
    name: "HeightTracker",
    component: HeightTracker,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/heightdatabase",
    name: "HeightDatabase",
    component: HeightDatabase,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
